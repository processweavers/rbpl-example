import org.scalastyle.sbt.ScalastylePlugin
import com.trueaccord.scalapb.compiler.Version.scalapbVersion

enablePlugins(SbtScalariform)
enablePlugins(SbtLicenseReport)
enablePlugins(ScalastylePlugin)

lazy val akkaHttpVersion = "10.0.11"
lazy val akkaVersion    = "2.5.7"

// Seems to be an ugly work around for obtaining a task where compile can depend on ...
lazy val compileScalastyle = taskKey[Unit]("compileScalastyle")
compileScalastyle := scalastyle.in(Compile).toTask("").value

lazy val releaseSettings = Seq(
  publishMavenStyle := true,
  pomIncludeRepository := { _ => false },
  publishArtifact in Test := false,
  publishTo := {
    val repo = "https://oss.sonatype.org/"
    if (isSnapshot.value)
      Some("snapshots" at repo + "content/repositories/snapshots")
    else
      Some("releases" at repo + "service/local/staging/deploy/maven2")
  },
  pomExtra :=
    <url>https://bitbucket.org/processweavers/rbpl/wiki/Home</url>
    <scm>
      <connection>scm:git:git://bitbucket.org/bitbucket.org/processweavers/rbpl-example.git</connection>
      <developerConnection>scm:git:ssh://bitbucket.org:processweavers/rbpl-example.git</developerConnection>
      <url>https://bitbucket.org/processweavers/rbpl-example/src</url>
    </scm>
    <licenses>
      <license>
        <name>BSD-3-Clause</name>
        <url>https://opensource.org/licenses/BSD-3-Clause</url>
        <distribution>repo</distribution>
      </license>
    </licenses>
    <developers>
      <developer>
        <name>Carsten Seibert</name>
        <email>seibert@seibertec.ch</email>
        <organization>seiberTEC GmbH</organization>
        <organizationUrl>http://www.seibertec.ch</organizationUrl>
      </developer>
      <developer>
        <name>Anke Seibert-Otto</name>
        <email>anke.seibert@seibertec.ch</email>
        <organization>seiberTEC GmbH</organization>
        <organizationUrl>http://www.seibertec.ch</organizationUrl>
      </developer>
    </developers>
)

lazy val ivyLocal = Resolver.file("ivyLocal", file(Path.userHome.absolutePath + "/.ivy2/local"))(Resolver.ivyStylePatterns)

lazy val root = (project in file("."))
  .settings(
      concurrentRestrictions in Global += Tags.limit(Tags.Test, 1),
      inThisBuild(List(
      organization    := "net.processweavers",
      version := "0.0.1",
      scalaVersion    := "2.12.4",
      scalacOptions ++= Seq(
        "-unchecked",
        "-deprecation",
        "-Xexperimental",
        "-feature",
        "-language:implicitConversions",
        "-language:reflectiveCalls"
      )
    )),
    (compile in Compile) := ((compile in Compile) dependsOn compileScalastyle).value,
    name := "rbpl-example",
    resolvers += Resolver.jcenterRepo,

    // TODO: ONLY FOR LOCAL TESTING OF LIBRARY REFACTORING!!!
    resolvers += ivyLocal,

    libraryDependencies ++= Seq(
      "net.processweavers" %% "rbpl-core" % "0.0.2",
      "net.processweavers" %% "akka-persistent-test-support" % "0.0.1-SNAPSHOT" % "test",
      "net.processweavers" %% "rbpl-core" % "0.0.2" % "protobuf",
      "com.trueaccord.scalapb" %% "scalapb-runtime"  % scalapbVersion  % "protobuf"
    ),
    cleanFiles += { baseDirectory.value / "persistent" }
  )
  .settings(releaseSettings: _*)

// Protobuf support in the respective sbt phases
PB.targets in Compile := Seq(
  scalapb.gen() -> (sourceManaged in Compile).value
)
PB.targets in Test := Seq(
  scalapb.gen() -> (sourceManaged in Test).value
)

// Only required if test has its own protobuf models file
// inConfig(Test)(sbtprotoc.ProtocPlugin.protobufConfigSettings)