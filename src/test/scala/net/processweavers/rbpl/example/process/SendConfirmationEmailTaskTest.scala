package net.processweavers.rbpl.example.process

import java.util.UUID

import akka.actor.{ ActorRef, ActorSystem, Props }
import akka.testkit.{ ImplicitSender, TestKit }
import net.processweavers.rbpl.core.Initializer
import net.processweavers.rbpl.core.task.{ TaskActivator, TaskDescriptor }
import net.processweavers.rbpl.core.testsupport.TaskTestScope
import net.processweavers.rbpl.example.Model.EmailAddress
import org.scalatest.{ Matchers, WordSpecLike }

class SendConfirmationEmailTaskTest extends TestKit(ActorSystem("as-SendConfirmationEmailTaskTest"))
  with WordSpecLike
  with ImplicitSender
  with Matchers {

  import SendConfirmationEmailTask._

  "A SendConfirmationEmailTask" must {
    "send the email on start and report back to the parent" in new TestScope {
      val task = newTask()
      val result = expectMsgType[SendConfirmationEmailTask.EmailSent]
      result.taskContext shouldBe taskContext
    }
  }

  abstract class TestScope(implicit activator: TaskActivator[EmailAddress]) extends TaskTestScope {
    def newTask(): ActorRef = {
      val i = Initializer("SendConfirmationEmailTask", EmailAddress("a@b.com"))
      val props = activator.propsFor(processContext, taskContext, i)
      system.actorOf(Props(classOf[EmSendConfirmationEmailTaskTestFixture], testActor +: props.args: _*), s"${i.name}-${UUID.randomUUID()}")
    }
  }
}

class EmSendConfirmationEmailTaskTestFixture(probe: ActorRef, taskDescriptor: TaskDescriptor, initializer: Initializer[EmailAddress])
  extends SendConfirmationEmailTask(taskDescriptor, initializer) {

  override protected def parentProcess: ActorRef = probe
}

