package net.processweavers.rbpl.example.process

import java.util.UUID

import akka.actor.{ ActorRef, ActorSystem, Props }
import akka.testkit.{ ImplicitSender, TestKit }
import net.processweavers.rbpl.core.Initializer
import net.processweavers.rbpl.core.task.{ TaskActivator, TaskDescriptor }
import net.processweavers.rbpl.core.testsupport.TaskTestScope
import net.processweavers.rbpl.example.Model.ConfirmationToken
import org.scalatest.{ Matchers, WordSpecLike }

class ExpectConfimationLinkClickedTaskTest extends TestKit(ActorSystem("as-ExpectConfimationLinkClickedTaskTest"))
  with WordSpecLike
  with ImplicitSender
  with Matchers {

  import ExpectConfimationLinkClickedTask._

  "A ExpectConfimationLinkClickedTask" must {

    "must accept (later) a REST endpoint and report when it was invoked with the proper token " in new TestScope {
      val task = newTask()
      val result = expectMsgType[ExpectConfimationLinkClickedTask.LinkClicked]
      result.taskContext shouldBe taskContext
    }

  }

  abstract class TestScope(implicit activator: TaskActivator[ConfirmationToken]) extends TaskTestScope {
    def newTask(): ActorRef = {
      val i = Initializer("ExpectConfimationLinkClickedTask", ConfirmationToken(UUID.randomUUID()))
      val props = activator.propsFor(processContext, taskContext, i)
      system.actorOf(Props(classOf[ExpectConfimationLinkClickedTaskTestFixture], testActor +: props.args: _*), s"${i.name}-${UUID.randomUUID()}")
    }
  }
}

class ExpectConfimationLinkClickedTaskTestFixture(probe: ActorRef, taskDescriptor: TaskDescriptor, initializer: Initializer[ConfirmationToken])
  extends ExpectConfimationLinkClickedTask(taskDescriptor, initializer) {
  override protected def parentProcess: ActorRef = probe
}

