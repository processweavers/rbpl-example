package net.processweavers.rbpl.example.process

import java.util.UUID
import java.util.concurrent.TimeUnit

import akka.actor.{ ActorRef, Props }
import akka.testkit.TestProbe
import net.processweavers.rbpl.core.Initializer
import net.processweavers.rbpl.core.process._
import net.processweavers.rbpl.core.task.{ ResultId, TimerTask }
import net.processweavers.rbpl.core.testsupport._
import net.processweavers.rbpl.example.Model.{ ConfirmationToken, EmailAddress }
import net.processweavers.testsupport.PersistentActorTestKit
import org.scalatest.Matchers

import scala.concurrent.duration.FiniteDuration

class EmailConfirmationProcessTest extends PersistentActorTestKit("as-EmailConfirmationProcessTest")
  with Matchers {

  "The EmailConfirmationProcess" must {

    "execute the happy flow" in new TestScope {

      val processActor = newProcess()

      // Initial task(s)
      val t1 = expectTaskStarted("SendConfirmationEmailTask")

      // Simulate result from SendConfirmationEmailTask
      processActor ! SendConfirmationEmailTask.EmailSent(ConfirmationToken(UUID.randomUUID()), t1.context, ResultId.create)
      expectTaskStopped(t1)
      val t2 = expectTaskStarted("ExpectConfimationLinkClickedTask")
      expectTimerStarted(EmailConfirmationProcess.ConfirmationTimerID)

      // Simulate result from ExpectConfimationLinkClickedTask
      processActor ! ExpectConfimationLinkClickedTask.LinkClicked(t2.context, ResultId.create)
      expectMsgAllOf(
        TaskStopped(t2.context.taskId),
        TimerStopped(EmailConfirmationProcess.ConfirmationTimerID),
        ProcessStopped(processContext.processId, processContext.correlationId))

      // finally ...
      dumpHistory(processActor, "-1")
    }

    "respect the timeout for accepting the confirmation link" in new TestScope {

      val processActor = newProcess()

      // Initial task(s)
      val t1 = expectTaskStarted("SendConfirmationEmailTask")

      // Simulate result from SendConfirmationEmailTask
      processActor ! SendConfirmationEmailTask.EmailSent(ConfirmationToken(UUID.randomUUID()), t1.context, ResultId.create)
      val t2 = expectTaskStarted("ExpectConfimationLinkClickedTask")
      val timer = expectTimerStarted(EmailConfirmationProcess.ConfirmationTimerID)

      // Simulate result from ExpectConfimationLinkClickedTask
      processActor ! TimerTask.TimerExpired(EmailConfirmationProcess.ConfirmationTimerID, timer.context, ResultId.create)
      expectMsgAllOf(
        TaskStopped(t2.context.taskId),
        TimerStopped(EmailConfirmationProcess.ConfirmationTimerID),
        ProcessStopped(processContext.processId, processContext.correlationId))

      // finally ...
      dumpHistory(processActor, "-2")
    }
  }

  val defaultStartData = EmailConfirmationProcess.StartData(EmailAddress("a@b.com"), FiniteDuration(500, TimeUnit.MILLISECONDS))

  abstract class TestScope(payload: EmailConfirmationProcess.StartData = defaultStartData)(implicit activator: ProcessActivator[EmailConfirmationProcess.StartData])
    extends ProcessTestScope {
    def newProcess(): ActorRef = {
      val props = activator.propsFor(processContext, Initializer("EmailConfirmationProcess", payload))
      system.actorOf(Props(classOf[EmailConfirmationProcessTestFixture], (processProbe.ref +: TestProbe().ref +: props.args): _*), s"EmailConfirmationProcess-${UUID.randomUUID()}")
    }
  }
}

// Mix-in the ProcessTestable for task monitoring
class EmailConfirmationProcessTestFixture(val probe: ActorRef, val genericTaskProbe: ActorRef, ctx: ProcessContext, initializer: Initializer[EmailConfirmationProcess.StartData])
  extends EmailConfirmationProcess(ctx, initializer) with ProcessTestable

