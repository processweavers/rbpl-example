package net.processweavers.rbpl.example.process.serialization

import java.util.UUID
import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.serialization.{ Serialization, SerializationExtension }
import akka.testkit.TestKit
import net.processweavers.rbpl.core.task.{ ResultId, TaskContext, TaskId }
import net.processweavers.rbpl.example.Model.{ ConfirmationToken, EmailAddress }
import net.processweavers.rbpl.example.models._
import net.processweavers.rbpl.example.process.{ EmailConfirmationProcess, ExpectConfimationLinkClickedTask, SendConfirmationEmailTask }
import org.scalatest.{ Matchers, WordSpecLike }

import scala.concurrent.duration.FiniteDuration

class ProtobufSerializerTest extends TestKit(ActorSystem("as-ProtobufSerializerTest"))
  with WordSpecLike
  with Matchers {

  lazy val serializerExt: Serialization = SerializationExtension(system)

  "A ProtobufSerializer" must {

    "handle EmailAddress" in {
      val subject = EmailAddress("a@c.com")
      ensureSerializationRoundtrip[EmailAddress, EmailAddressPersisted](subject)
    }

    "handle ConfirmationToken" in {
      val subject = ConfirmationToken(UUID.randomUUID())
      ensureSerializationRoundtrip[ConfirmationToken, ConfirmationTokenPersisted](subject)
    }

    "handle EmailConfirmationProcess.StartData" in {
      val subject = EmailConfirmationProcess.StartData(EmailAddress("a@c.com"), FiniteDuration(1, TimeUnit.DAYS))
      ensureSerializationRoundtrip[EmailConfirmationProcess.StartData, EmailConfirmationProcessStartDataPersisted](subject)
    }

    "handle SendConfirmationEmailTask.EmailSent" in {
      val subject = SendConfirmationEmailTask.EmailSent(
        ConfirmationToken(UUID.randomUUID()),
        taskId,
        ResultId.create)
      ensureSerializationRoundtrip[SendConfirmationEmailTask.EmailSent, SendConfirmationEmailTaskEmailSentPersisted](subject)
    }

    "handle ExpectConfimationLinkClickedTask.LinkClicked" in {
      val subject = ExpectConfimationLinkClickedTask.LinkClicked(
        taskId, ResultId.create)
      ensureSerializationRoundtrip[ExpectConfimationLinkClickedTask.LinkClicked, ExpectConfimationLinkClickedTaskLinkClickedPersisted](subject)
    }

  }

  def ensureSerializationRoundtrip[MODEL <: AnyRef, PERSREP](subject: MODEL)(implicit mp: Function1[MODEL, PERSREP], pm: Function1[PERSREP, MODEL]): Unit = {
    ensureSubjectIsRegistered(subject)
    val persistenRepresentation: PERSREP = subject
    val rehydrated: MODEL = persistenRepresentation
    rehydrated shouldBe subject
  }

  def ensureSubjectIsRegistered(subject: AnyRef): Unit = {
    val s = serializerExt.findSerializerFor(subject)
    s.getClass shouldBe classOf[ProtobufSerializer]
  }

  val taskId = TaskContext(TaskId.create, Some(ResultId.create), Some("transition"))

}
