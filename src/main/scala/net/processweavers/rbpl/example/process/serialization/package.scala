package net.processweavers.rbpl.example.process

import java.util.UUID
import java.util.concurrent.TimeUnit

import net.processweavers.rbpl.example.Model.{ ConfirmationToken, EmailAddress }
import net.processweavers.rbpl.example.models._

import scala.concurrent.duration.FiniteDuration

package object serialization {

  import net.processweavers.rbpl.core.process.serialization._

  implicit def fromEmailAddress(t: EmailAddress): EmailAddressPersisted = EmailAddressPersisted(t.address)
  implicit def fromEmailAddressPersisted(u: EmailAddressPersisted): EmailAddress = EmailAddress(u.emailAddress)

  implicit def fromConfirmationToken(t: ConfirmationToken): ConfirmationTokenPersisted = ConfirmationTokenPersisted(t.token.toString)
  implicit def fromConfirmationTokenPersisted(u: ConfirmationTokenPersisted): ConfirmationToken = ConfirmationToken(UUID.fromString(u.token))

  implicit def fromEmailConfirmationProcessStartData(model: EmailConfirmationProcess.StartData): EmailConfirmationProcessStartDataPersisted =
    EmailConfirmationProcessStartDataPersisted(
      Some(model.emailAddress),
      model.awaitDuration.toMillis)

  implicit def fromEmailConfirmationProcessStartDataPersisted(persRep: EmailConfirmationProcessStartDataPersisted): EmailConfirmationProcess.StartData =
    EmailConfirmationProcess.StartData(
      persRep.emailAddress.get,
      FiniteDuration(persRep.awaitDuration, TimeUnit.MILLISECONDS))

  implicit def fromSendConfirmationEmailTaskEmailSent(t: SendConfirmationEmailTask.EmailSent): SendConfirmationEmailTaskEmailSentPersisted =
    SendConfirmationEmailTaskEmailSentPersisted(
      Some(t.token),
      Some(t.taskContext),
      t.resultId)

  implicit def fromSendConfirmationEmailTaskEmailSentPersisted(persRep: SendConfirmationEmailTaskEmailSentPersisted): SendConfirmationEmailTask.EmailSent =
    SendConfirmationEmailTask.EmailSent(
      persRep.token.get,
      persRep.taskContext.get,
      persRep.resultId)

  implicit def fromExpectConfimationLinkClickedTaskLinkClicked(m: ExpectConfimationLinkClickedTask.LinkClicked): ExpectConfimationLinkClickedTaskLinkClickedPersisted =
    ExpectConfimationLinkClickedTaskLinkClickedPersisted(
      Some(m.taskContext),
      m.resultId)

  implicit def fromExpectConfimationLinkClickedTaskLinkClickedPersisted(persRep: ExpectConfimationLinkClickedTaskLinkClickedPersisted): ExpectConfimationLinkClickedTask.LinkClicked =
    ExpectConfimationLinkClickedTask.LinkClicked(
      persRep.taskContext.get,
      persRep.resultId)
}
