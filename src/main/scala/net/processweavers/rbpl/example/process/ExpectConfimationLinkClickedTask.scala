package net.processweavers.rbpl.example.process

import java.util.concurrent.TimeUnit

import akka.actor.{ Actor, ActorLogging, Props }
import net.processweavers.rbpl.core.Initializer
import net.processweavers.rbpl.core.process.ProcessContext
import net.processweavers.rbpl.core.task._
import net.processweavers.rbpl.example.Model.ConfirmationToken

import scala.concurrent.duration.FiniteDuration

class ExpectConfimationLinkClickedTask(val descriptor: TaskDescriptor, i: Initializer[ConfirmationToken]) extends Actor
  with Task
  with ActorLogging {

  import ExpectConfimationLinkClickedTask._

  def receive: Receive = {

    // todo: Imagine a proper implementation ;)
    case Initialize =>
      import context.dispatcher
      context.system.scheduler.scheduleOnce(FiniteDuration(200, TimeUnit.MILLISECONDS), self, EndpointInvoked(i.payload))

    case EndpointInvoked(t) if t == i.payload =>
      parentProcess ! LinkClicked(descriptor.context, ResultId.create)

    // todo: Handle more sophisticated
    case EndpointInvoked(t) =>
      log.warning("Wrong token received! token={}", t)
  }
}

object ExpectConfimationLinkClickedTask {

  case class EndpointInvoked(token: ConfirmationToken)

  case class LinkClicked(taskContext: TaskContext, resultId: ResultId) extends TaskResult {
    override def description: String = "LinkClicked"
  }

  implicit val ExpectConfimationLinkClickedTaskActivator = new TaskActivator[ConfirmationToken] {
    def propsFor(processContext: ProcessContext, taskContext: TaskContext, i: Initializer[ConfirmationToken])(): Props =
      Props(classOf[ExpectConfimationLinkClickedTask], TaskDescriptor(i.name, processContext, taskContext, i.description), i)
  }

}
