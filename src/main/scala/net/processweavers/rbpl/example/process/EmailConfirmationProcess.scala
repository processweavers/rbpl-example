package net.processweavers.rbpl.example.process

import akka.actor.{ Actor, Props }
import net.processweavers.rbpl.core.Initializer
import net.processweavers.rbpl.core.process._
import net.processweavers.rbpl.example.Model.EmailAddress
import shapeless.HNil
import SendConfirmationEmailTask._
import ExpectConfimationLinkClickedTask._
import net.processweavers.rbpl.core.task.TimerTask

import scala.concurrent.duration.FiniteDuration

class EmailConfirmationProcess(val ctx: ProcessContext, val initializer: Initializer[EmailConfirmationProcess.StartData])
  extends BaseProcess(Initializer[EmailAddress](EmailConfirmationProcess.SendConfirmationEmailTaskName, initializer.payload.emailAddress) :: HNil, Some(initializer)) {

  import AndThenAPI._
  import EmailConfirmationProcess._

  def predicateArgument: Any = None

  def receiveRecover: Receive = Actor.emptyBehavior

  def receiveCommand: Receive = {

    case r: SendConfirmationEmailTask.EmailSent =>
      persistAndHandleResult(r) { res =>
        log.info("SendConfirmationEmailTask - result: {}", res)
      } andThen (
        StopCurrentTask(),
        StartNewTask(
          "confirmation email sent",
          Initializer(ExpectConfimationLinkClickedTaskName, r.token)),
          StartTimer(ConfirmationTimerID, initializer.payload.awaitDuration))

    case r: ExpectConfimationLinkClickedTask.LinkClicked =>
      persistAndHandleResult(r) { res =>
        log.info("PROCESS SUCESSFUL: ExpectConfimationLinkClickedTask - result: {}", res)
      } andThen (
        StopCurrentTask(),
        StopTimer(EmailConfirmationProcess.ConfirmationTimerID),
        StopProcess())

    case r @ TimerTask.TimerExpired(`ConfirmationTimerID`, _, _) =>
      persistAndHandleResult(r) { res =>
        log.info("PROCESS FAILURE: TimerTask.TimerExpired - result: {}", res)
      } andThen (
        StopTask(ExpectConfimationLinkClickedTaskName),
        StopTimer(EmailConfirmationProcess.ConfirmationTimerID),
        StopProcess())
  }
}

object EmailConfirmationProcess {

  val ConfirmationTimerID = "ConfirmationTimer"
  val ExpectConfimationLinkClickedTaskName = "ExpectConfimationLinkClickedTask"
  val SendConfirmationEmailTaskName = "SendConfirmationEmailTask"

  case class StartData(emailAddress: EmailAddress, awaitDuration: FiniteDuration)

  implicit val activator = new ProcessActivator[StartData] {
    def propsFor(context: ProcessContext, initializer: Initializer[StartData])(): Props = Props(classOf[EmailConfirmationProcess], context, initializer)
  }
}
