package net.processweavers.rbpl.example.process.serialization

import akka.actor.ExtendedActorSystem
import net.processweavers.rbpl.core.process.serialization.BaseProcessSerializer
import net.processweavers.rbpl.example.Model.{ ConfirmationToken, EmailAddress }
import net.processweavers.rbpl.example.models._
import net.processweavers.rbpl.example.process.{ EmailConfirmationProcess, ExpectConfimationLinkClickedTask, SendConfirmationEmailTask }

class ProtobufSerializer(system: ExtendedActorSystem) extends BaseProcessSerializer(system) {

  override val identifier: Int = 9100

  val ms: Map[String, PersistanceMapper[_, _]] = Map(
    classOf[EmailConfirmationProcess.StartData].getName ->
      new PersistanceMapper[EmailConfirmationProcess.StartData, EmailConfirmationProcessStartDataPersisted](EmailConfirmationProcessStartDataPersisted.parseFrom),
    classOf[EmailAddress].getName ->
      new PersistanceMapper[EmailAddress, EmailAddressPersisted](EmailAddressPersisted.parseFrom),
    classOf[ConfirmationToken].getName ->
      new PersistanceMapper[ConfirmationToken, ConfirmationTokenPersisted](ConfirmationTokenPersisted.parseFrom),
    classOf[SendConfirmationEmailTask.EmailSent].getName ->
      new PersistanceMapper[SendConfirmationEmailTask.EmailSent, SendConfirmationEmailTaskEmailSentPersisted](SendConfirmationEmailTaskEmailSentPersisted.parseFrom),
    classOf[ExpectConfimationLinkClickedTask.LinkClicked].getName ->
      new PersistanceMapper[ExpectConfimationLinkClickedTask.LinkClicked, ExpectConfimationLinkClickedTaskLinkClickedPersisted](ExpectConfimationLinkClickedTaskLinkClickedPersisted.parseFrom))

}

