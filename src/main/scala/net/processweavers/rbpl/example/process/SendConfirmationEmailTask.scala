package net.processweavers.rbpl.example.process

import java.util.UUID

import akka.actor.{ Actor, ActorLogging, ActorRef, Props }
import net.processweavers.rbpl.core.Initializer
import net.processweavers.rbpl.core.process.ProcessContext
import net.processweavers.rbpl.core.task._
import net.processweavers.rbpl.example.Model.{ ConfirmationToken, EmailAddress }

class SendConfirmationEmailTask(val descriptor: TaskDescriptor, i: Initializer[EmailAddress]) extends Actor
  with ActorLogging
  with Task {

  import SendConfirmationEmailTask._

  def receive: Receive = {
    case Initialize =>
      val token = ConfirmationToken(UUID.randomUUID())
      log.info("\nto: {}\nDear user, please click on the link below to confirm your email address.\nhttp://localhost:XXX/{}", i.payload.address, token.token.toString)
      parentProcess ! EmailSent(token, descriptor.context, ResultId.create)
  }
}

object SendConfirmationEmailTask {

  case class EmailSent(token: ConfirmationToken, taskContext: TaskContext, resultId: ResultId) extends TaskResult {
    override def description: String = s"EmailSent(token=$token)"
  }

  implicit val SendConfirmationEmailTaskActivator = new TaskActivator[EmailAddress] {
    def propsFor(processContext: ProcessContext, taskContext: TaskContext, i: Initializer[EmailAddress])(): Props =
      Props(classOf[SendConfirmationEmailTask], TaskDescriptor(i.name, processContext, taskContext, i.description), i)
  }

}
