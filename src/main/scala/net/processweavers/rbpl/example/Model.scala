package net.processweavers.rbpl.example

import java.util.UUID

object Model {
  case class EmailAddress(address: String)
  case class ConfirmationToken(token: UUID)
}
